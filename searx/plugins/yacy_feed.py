"""
To be able to export those sweet results, first and foremost you gotta be sure that
the directory under the global var export_directory is
1) correctly set
2) is readable, writable and executable for everyone

So you just have to do a nice
# chmod 777 -R /usr/share/yacy/DATA/SURROGATES/in/
and it should be okay.
"""

from datetime import datetime
from os.path import join
from uuid import uuid4
from xml.sax.saxutils import escape
from xml.etree.ElementTree import Element, ElementTree

from searx import settings, logger, searx_debug

name = 'Yacy Feeder'
description = 'Feeds the returned results to a Yacy instance, instance that is gonna index all those pages.'
default_on = False  # disabled by default
export_directory = '/usr/share/yacy/DATA/SURROGATES/in/'  # basically where you put your yacy instance.
if searx_debug:
    export_directory = '/tmp/'


def write_xml(xml: Element):
    """Write this dict as a XML file"""

    xml_file_name = join(export_directory, "{}.xml".format(str(uuid4())))
    xml_file = ElementTree(xml)
    xml_file.write(xml_file_name)


def on_result(request, search, result):
    """Hook that is called for every new returned result."""

    root_xml = Element('surrogates', attrib={"xmlns:dc": "http://purl.org/dc/elements/1.1/"})
    record_node = Element('record')
    root_xml.append(record_node)

    title_node = Element('dc:title')
    title_node.text = escape(result['title'])

    identifier_node = Element('dc:identifier')
    identifier_node.text = escape(result['url'])

    language_node = Element('dc:lang')
    language_node.text = search.search_query.lang

    identifier_node = Element('dc:identifier')
    identifier_node.text = escape(result['url'])

    date_node = Element('dc:date')
    date_node.text = datetime.now().isoformat()

    # some results, like the one who come from wikidat, do not contain the "content" key
    if 'content' in result:
        description_node = Element('dc:description')
        description_node.text = escape(result['content'])
        record_node.append(description_node)

    record_node.append(title_node)
    record_node.append(identifier_node)
    record_node.append(date_node)
    record_node.append(language_node)

    try:
        write_xml(root_xml)
    except PermissionError:
        logger.critical("Please be sure that {} permissions are set to 777.".format(export_directory))
        return False
    return True
